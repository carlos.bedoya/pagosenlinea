package com.pagosenlinea.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A DTO for the {@link com.pagosenlinea.domain.Pago} entity.
 */
public class PagoDTO implements Serializable {

    private Long id;

    private Double precio;

    private ZonedDateTime horaPedido;

    private String state;

    private Long clientId;

    private Integer ref;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public ZonedDateTime getHoraPedido() {
        return horaPedido;
    }

    public void setHoraPedido(ZonedDateTime horaPedido) {
        this.horaPedido = horaPedido;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Integer getRef() {
        return ref;
    }

    public void setRef(Integer ref) {
        this.ref = ref;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PagoDTO)) {
            return false;
        }

        PagoDTO pagoDTO = (PagoDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, pagoDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PagoDTO{" +
            "id=" + getId() +
            ", precio=" + getPrecio() +
            ", horaPedido='" + getHoraPedido() + "'" +
            ", state='" + getState() + "'" +
            ", clientId=" + getClientId() +
            ", ref=" + getRef() +
            "}";
    }
}
