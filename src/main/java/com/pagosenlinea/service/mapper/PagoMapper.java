package com.pagosenlinea.service.mapper;

import com.pagosenlinea.domain.*;
import com.pagosenlinea.service.dto.PagoDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Pago} and its DTO {@link PagoDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PagoMapper extends EntityMapper<PagoDTO, Pago> {}
