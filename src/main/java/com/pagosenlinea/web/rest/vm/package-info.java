/**
 * View Models used by Spring MVC REST controllers.
 */
package com.pagosenlinea.web.rest.vm;
