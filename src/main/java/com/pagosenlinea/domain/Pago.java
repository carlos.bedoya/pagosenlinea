package com.pagosenlinea.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Pago.
 */
@Entity
@Table(name = "pago")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Pago implements Serializable {

    
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "precio")
    private Double precio;

    @Column(name = "hora_pedido")
    private ZonedDateTime horaPedido;

    @Column(name = "state")
    private String state;

    @Column(name = "client_id")
    private Long clientId;

    @Column(name = "ref")
    private Integer ref;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Pago id(Long id) {
        this.id = id;
        return this;
    }

    public Double getPrecio() {
        return this.precio;
    }

    public Pago precio(Double precio) {
        this.precio = precio;
        return this;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public ZonedDateTime getHoraPedido() {
        return this.horaPedido;
    }

    public Pago horaPedido(ZonedDateTime horaPedido) {
        this.horaPedido = horaPedido;
        return this;
    }

    public void setHoraPedido(ZonedDateTime horaPedido) {
        this.horaPedido = horaPedido;
    }

    public String getState() {
        return this.state;
    }

    public Pago state(String state) {
        this.state = state;
        return this;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getClientId() {
        return this.clientId;
    }

    public Pago clientId(Long clientId) {
        this.clientId = clientId;
        return this;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Integer getRef() {
        return this.ref;
    }

    public Pago ref(Integer ref) {
        this.ref = ref;
        return this;
    }

    public void setRef(Integer ref) {
        this.ref = ref;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Pago)) {
            return false;
        }
        return id != null && id.equals(((Pago) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Pago{" +
            "id=" + getId() +
            ", precio=" + getPrecio() +
            ", horaPedido='" + getHoraPedido() + "'" +
            ", state='" + getState() + "'" +
            ", clientId=" + getClientId() +
            ", ref=" + getRef() +
            "}";
    }
}
