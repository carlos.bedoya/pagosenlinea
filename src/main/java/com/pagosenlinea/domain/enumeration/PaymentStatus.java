

package com.pagosenlinea.domain.enumeration;

public enum PaymentStatus {
    PENDING, FINISHED, CANCELED, FAILED
}